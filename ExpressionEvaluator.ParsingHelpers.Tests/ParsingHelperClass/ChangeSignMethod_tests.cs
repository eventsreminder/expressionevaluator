﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentAssertions;

namespace ExpressionEvaluator.ParsingHelpers.Tests.ParsingHelperClass
{
    [TestClass]
    public class ChangeSignMethod_tests
    {
        [TestMethod]
        public void ChangeSignMethod_should_change_sign_of_a_variable()
        {
            ParsingHelper.ChangeSign("+a").Should().Be("-a");
            ParsingHelper.ChangeSign("-b").Should().Be("b");
            ParsingHelper.ChangeSign("c").Should().Be("-c");
        }

        [TestMethod]
        public void ChangeSignMethod_should_change_sign_of_a_variable_in_power()
        {
            ParsingHelper.ChangeSign("+aa^2").Should().Be("-aa^2");
            ParsingHelper.ChangeSign("-bb^2").Should().Be("bb^2");
            ParsingHelper.ChangeSign("cc^2").Should().Be("-cc^2");
        }

        [TestMethod]
        public ChangeSignMethod_test()
        {
            ParsingHelper.ChangeSign("a").Should().Be("-a");
            ParsingHelper.ChangeSign("-a").Should().Be("+a");
            ParsingHelper.ChangeSign("+a").Should().Be("-a");
            ParsingHelper.ChangeSign("abc^2").Should().Be("-abc^2");
            ParsingHelper.ChangeSign("a+b-c").Should().Be("-a-b+c");
            ParsingHelper.ChangeSign("a+(b-(c-d))-fe^10").Should().Be("-a-(b-(c-d))+fe^10 ");
        }
    }
}
