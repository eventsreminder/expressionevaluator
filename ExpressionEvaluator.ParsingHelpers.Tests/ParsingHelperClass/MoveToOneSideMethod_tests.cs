﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using ExpressionEvaluator.ParsingHelpers;

namespace ExpressionEvaluator.ParsingHelpers.Tests
{
    [TestClass]
    public class MoveToOneSideMethod
    {
        [TestMethod]
        public void MoveToOneSide_should_remove_equals_sign()
        {
            ParsingHelper.MoveToOneSide("something=something")
                .Should()
                .NotContain("=");
        }

        [TestMethod]
        public void MoveToOneSide_should_work_as_expected()
        {
            ParsingHelper.MoveToOneSide("something=something")
                .Should()
                .Be("something-(something)");
        }
    }
}
