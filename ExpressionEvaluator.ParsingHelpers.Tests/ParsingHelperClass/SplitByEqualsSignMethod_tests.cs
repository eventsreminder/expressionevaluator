﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;

namespace ExpressionEvaluator.ParsingHelpers.Tests.ParsingHelperClass
{
    [TestClass]
    public class SplitByEqualsSignMethod_tests
    {
        [TestMethod]
        public void SplitByEqualsSign_should_work_as_expected()
        {
            var parts = ParsingHelper.SplitByEqualsSign("something=something");
            parts.Count().Should().Be(3);
            parts[1].Should().Be("=");
        }
    }
}
