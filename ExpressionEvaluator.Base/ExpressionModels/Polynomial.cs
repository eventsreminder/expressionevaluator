﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExpressionEvaluator.Domain
{
    public class Polynomial: IExpressionObject
    {
        public IEnumerable<Monomial> Monomials { get; private set; }

        public Polynomial(IEnumerable<Monomial> monomials)
        {
            Monomials = monomials;
        }

        public void Simplify()
        {
            throw new NotImplementedException();
        }

        public string ToExpressionString()
        {
            throw new NotImplementedException();
        }
    }
}
