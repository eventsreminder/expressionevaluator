﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExpressionEvaluator.Domain
{
    public class Variable : IExpressionObject
    {
        public int Power { get; private set; }
        public string VariableName { get; private set; }

        public Variable(int power, string variableName)
        {
            if (power < 0)
                throw new Exception("Не удалось упростить выпажение. Найдена отрицательная степень.");

            var allowableLetters =  //refactoring: положить куда-нибудь чтобы не было magic string
                "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
            if (!allowableLetters.Contains(variableName.ToString()))
                throw new Exception("Не удалось упростить выражение. В названии переменной найден некорректный символ \"" + variableName + "\"");

            Power = power;
            VariableName = variableName;
        }

        public void Simplify()
        {
            throw new NotImplementedException();
        }

        public string ToExpressionString()
        {
            if (Power == 1) return VariableName.ToString();
            if (Power > 1) return VariableName.ToString() + "^" + Power.ToString();
            if (Power == 0) return String.Empty;

            throw new Exception("Не удалось упростить выражение. Степень неправильная.");
        }

        public override bool Equals(object obj)
        {
            var other = obj as Variable;

            if (other == null) return false;

            return Power == other.Power && VariableName == other.VariableName;
        }

    }
}
