﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExpressionEvaluator.Domain
{
    public class Monomial : IExpressionObject
    {
        //private bool _Sign;

        private IList<float> _Coefficients;
        public IEnumerable<float> Coefficients { get { return _Coefficients; } }
        
        public IEnumerable<Variable> Variables { get; private set; }

        public Monomial(IEnumerable<float> coefficients, IEnumerable<Variable> variables)
        {
            _Coefficients = new List<float>(coefficients ?? Enumerable.Empty<float>());
            _Coefficients.Add(1);
            Variables = variables ?? Enumerable.Empty<Variable>();
        }

        private void MultiplyFactors()
        {
            var groupedVariables = Variables
                .GroupBy(v => v)
                .Select(g => new KeyValuePair<Variable,int>(g.Key,g.Count()));

            foreach (var groupedViriable in groupedVariables)
            {
                _Coefficients.Add(groupedViriable.Value);
            }

            Variables = groupedVariables.Select(kvp => kvp.Key);
            
            var aggregatedCoefficient = _Coefficients
                .Aggregate<float>((c1,c2) => c1*c2);

            _Coefficients = new List<float>(new float[] { aggregatedCoefficient });
        }

        public void Simplify()
        {
            MultiplyFactors();
        }

        public string ToExpressionString()
        {
            return _Coefficients.First().ToString("0.00")  // положиь формат в app.config или еще куда то
                + String.Join(
                    ""
                    , Variables.Select(
                        v =>
                        {
                            v.Simplify();
                            return v.ToExpressionString();
                        }
                        )
                    );
        }
    }
}
