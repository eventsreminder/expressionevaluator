﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExpressionEvaluator.Domain
{
    public interface IMonomialParser
    {
        IEnumerable<string> SplitIntoElementaryFactorStrings(string monomialString);
    }
}
