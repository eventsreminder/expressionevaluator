﻿using DryIoc;
using ExpressionEvaluator.Domain;
using ExpressionEvaluator.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExpressionEvaluator.Initializer
{
    public class Bootstrapper
    {
        public static void Run(IContainer container)
        {
            container.Register<IPolynomialAssembler, PolynomialAssembler>(Reuse.Transient);
        }
    }
}
