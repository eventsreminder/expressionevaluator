﻿using DryIoc;
using ExpressionEvaluator.Domain;
using ExpressionEvaluator.Initializer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExpressionEvaluator
{
    public class Program
    {
        private static IPolynomialAssembler _PolynomialAssembler;

        static void Main(string[] args)
        {
            var container = new Container();
            Bootstrapper.Run(container);
            _PolynomialAssembler = container.Resolve<IPolynomialAssembler>();
        }

        private static void SimplifyExpression(string polynomialString)
        {
            var polynomial = _PolynomialAssembler.ToPolynomial(polynomialString);
            polynomial.Simplify();
            var result = polynomial.ToExpressionString();
        }
    }
}
