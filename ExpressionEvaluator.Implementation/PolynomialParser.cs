﻿using ExpressionEvaluator.Domain;
using ExpressionEvaluator.ParsingHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ExpressionEvaluator.Implementation
{
    public class PolynomialParser: IPolynomialParser
    {
        public IEnumerable<string> SplitIntoMonomialStrings(string polynomialString)
        {
            polynomialString = ParsingHelper.ReplaceUnnecessaryChars(polynomialString);

            var allowableLetters =  //refactoring: положить строку в App.Config или еще куда
                "0123456789"
                + "^.+-()="
                + "qwertyuiopasdfghjklzxcvbnm"
                + "QWERTYUIOPASDFGHJKLZXCVBNM";

            ThrowExceptionIfInvalidCharacters(polynomialString, allowableLetters);

            polynomialString = ParsingHelper.MoveToOneSide(polynomialString);

            var monomialStrings = ParsingHelper.OpenBrackets(polynomialString)
                .SelectMany(s => s.Split('+','-'));

            return monomialStrings;
        }

        private void ThrowExceptionIfInvalidCharacters(string polynomialString, string allowableLetters)
        {
            foreach (char c in polynomialString)
            {
                if (!allowableLetters.Contains(c.ToString()))
                    throw new Exception("Не удалось упростить выражение. Не предусмотрена работа с выражениями, содержащими символ \"" + c + "\"");
            }
        }

        
    }
}
