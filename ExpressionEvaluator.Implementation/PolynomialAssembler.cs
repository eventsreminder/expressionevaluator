﻿using ExpressionEvaluator.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ExpressionEvaluator.Implementation
{
    public class PolynomialAssembler: IPolynomialAssembler
    {
        private readonly IPolynomialParser _PolynomialParser;
        private readonly IMonomialParser _MonomialParser;

        public PolynomialAssembler(IPolynomialParser polynomialParser, IMonomialParser monomialParser)
        {
            _PolynomialParser = polynomialParser;
            _MonomialParser = monomialParser;
        }

        public Polynomial ToPolynomial(string polynomialString)
        {
            List<Monomial> monomialsList = new List<Monomial>();

            var monomialStrings = _PolynomialParser.SplitIntoMonomialStrings(polynomialString);

            foreach (var monomialString in monomialStrings)
            {
                var elementaryFactorStrings = _MonomialParser.SplitIntoElementaryFactorStrings(monomialString);

                var IsPositive = !monomialString.Contains("-");

                float f;
                var coefficients = elementaryFactorStrings
                    .Where(s => float.TryParse(s, out f))
                    .Select(s => float.Parse(s)).ToList();
                if (!IsPositive)
                    coefficients.Add(-1);

                var varsInPowerOfOneChars = elementaryFactorStrings
                    .Where(s => s.Length == 1 && "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM".Contains(s[0]))
                    .Select(s => s[0])
                    .ToString();

                var varsInPowerKeyValuePairs = elementaryFactorStrings
                    .Where(s => s.Contains("^"))
                    .Select(
                        s =>
                        {
                            var variable = s.FirstOrDefault(c => "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM".Contains(c)).ToString();

                            if (variable == null)
                                throw new Exception("Не удалось упростить выражение. Ожидалось выражение типа x^n но не было найдено переменной типа х");

                            int power;
                            bool parsed = int.TryParse(Regex.Match(s, @"^\d+$").Value, out power);

                            if (!parsed)
                                throw new Exception("Не удалось упростить выражение. Ожидалось целое число.");

                            return new KeyValuePair<string, int>(variable, power);
                        }
                        );

                var variables = varsInPowerKeyValuePairs
                    .Select(kvp => new Variable(kvp.Value,kvp.Key))
                    .Union(
                        varsInPowerOfOneChars
                        .Select(c => new Variable(1,c.ToString()))
                    );

                monomialsList.Add(new Monomial(coefficients, variables));
            }

            return new Polynomial(monomialsList);
        }
    }
}
