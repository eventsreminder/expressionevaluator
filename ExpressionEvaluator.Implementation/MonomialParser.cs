﻿using ExpressionEvaluator.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ExpressionEvaluator.Implementation
{
    public class MonomialParser: IMonomialParser
    {
        public IEnumerable<string> SplitIntoElementaryFactorStrings(string monomialString)
        {
            var regex = new Regex(@"((a-zA-Z)|((a-zA-Z)\^(\d+))|(\d+\.\d+))", RegexOptions.IgnorePatternWhitespace);

            foreach (Match capture in regex.Matches(monomialString))
            {
                yield return capture.Value;
            }
        }
    }
}
