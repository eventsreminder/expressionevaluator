﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ExpressionEvaluator.ParsingHelpers
{
    public class ParsingHelper
    {
        public static string ReplaceUnnecessaryChars(string inputString)
        {
            return inputString
                .Replace(" ", "")
                .Replace("*", "");
        }

        public static string MoveToOneSide(string polynomialString)
        {
            var parts = SplitByEqualsSign(polynomialString);

            var equalSignCount = parts.Where(x => x == "=").Count();

            if (equalSignCount == 0 || equalSignCount > 1 || parts[1] != "=" || parts.Count() != 3)
                throw new Exception("Не удалось упростить выражение. Возможно, количество знаков равно либо больше либо меньше 1.");

            var result = parts[0];

            result += "-(" + parts[2] + ")";

            return result;
        }

        public static string[] SplitByEqualsSign(string inputString)
        {
            return Regex.Split(inputString, @"(\=)");
        }

        public static IEnumerable<string> OpenBrackets(string polynomialString)
        {
            var bracketsContents = MatchParenthesesContent(polynomialString);
            string cutExpression = polynomialString;

            foreach (var bracketsExpression in bracketsContents)
            {
                cutExpression = cutExpression.Replace(bracketsExpression, "");
            }

            if (!String.IsNullOrWhiteSpace(cutExpression))
                yield return cutExpression;

            foreach (var bracketsExpression in bracketsContents)
            {
                foreach (var deeperBracketExpression in OpenBrackets(TrimBrackets(bracketsExpression)))
                {
                    yield return deeperBracketExpression;
                }
            }
        }

        public static IEnumerable<string> MatchParenthesesContent(string polynomialString)
        {
            var regex = new Regex(@"
                [\+\(|\-\(|\(]        # Match ( or +( or -(
                (
                    [^()]+            # all chars except ()
                    | (?<Level>\()    # or if ( then Level += 1
                    | (?<-Level>\))   # or if ) then Level -= 1
                )+                    # Repeat (to go from inside to outside)
                (?(Level)(?!))        # zero-width negative lookahead assertion
                \)                    # Match )",
                RegexOptions.IgnorePatternWhitespace);

            foreach (Match capture in regex.Matches(polynomialString))
            {
                yield return capture.Value;
            }
        }

        public static string TrimBrackets(string polynomialWithBrackets)
        {
            string result;
            if (polynomialWithBrackets[0] == '+')
                result = polynomialWithBrackets.Substring(1).Trim('(', ')');
            else if (polynomialWithBrackets[0] == '-')
                result = ChangeSign(polynomialWithBrackets.Substring(1).Trim('(', ')'));
            else result = polynomialWithBrackets.Trim('(', ')');
            return result;
        }

        public static string ChangeSign(string polynomialString)
        {
            string result = "";
            int index = 0;
            while (index <= polynomialString.Length)
            {
                if (polynomialString[index] == '(')
                    break;
                else if (polynomialString[index] == ')
            }

//            string expressionStart =
//                @"(?<!     
//                 \(        
//                 (^(\))*)   
//                )          
//                ";
//            string plus = @"(\+)";
//            string minus = @"(\-)";
//            string temporaryMinus = @"(\_)";
//            string expressionEnd =
//                @"
//                (?!       
//                 (^(\))*)   
//                 \)        
//                )          
//                "
//                ;

//            var plusesAlmostReplaced = Regex.Replace(polynomialString, expressionStart + plus + expressionEnd, "_");
//            var minusesReplaced = Regex.Replace(plusesAlmostReplaced, expressionStart + minus + expressionEnd, "+");
//            return Regex.Replace(minusesReplaced, expressionStart + temporaryMinus + expressionEnd, "-");
        }
    }
}
